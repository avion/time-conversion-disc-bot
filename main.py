import yaml
import discord
import datefinder
import logging
import re
import pytz
from datetime import datetime
import random
from simpledate import SimpleDate

logging.basicConfig(level=logging.INFO)

# Read config file
with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.Loader)

# Set up time zone residents
pdt_residents = config["time_zone"]["pdt"]
cdt_residents = config["time_zone"]["cdt"]
edt_residents = config["time_zone"]["edt"]
pht_residents = config["time_zone"]["pht"]

keyword1_message = config["keyword1_message"]
keyword2_message = config["keyword2_message"]
keyword4_message = config["keyword4_message"]
keyword5_message = config["keyword5_message"]
keyword6_message = config["keyword6_message"]
keyword7_message = config["keyword7_message"]
keyword8_message = config["keyword8_message"]
keyword9_message = config["keyword9_message"]
keyword10_message = config["keyword10_message"]
keyword11_message = config["keyword11_message"]


class MyClient(discord.Client):
    async def on_ready(self):
        print('We have logged in as {0.user}'.format(client))

    async def on_message(self, message):
        time_re = ""
        is_bot_command = False

        # Ignore message if the bot was the sender
        if message.author.id == self.user.id:
            return
        else:
            # See if there's time in the message using regex
            time_re = re.search(
                r'\d{1,2}\s?(?:(?:am|pm)|(?::\d{1,2}\s?)(?:am|pm)?)',
                message.content.lower())

        # Respond to $time command
        if message.content.startswith(f'{config["command_prefix"]}time'):
            time_re = datetime.now()
            is_bot_command = True

        #print(f'time_re: {time_re}')
        if time_re is not None:
            try:
                if is_bot_command is False:
                    time_parse = list(
                        datefinder.find_dates(str(message.content.lower())))
                else:
                    time_parse = list(datefinder.find_dates(str(time_re)))
                print(f'time_parse: {time_parse}')

                if is_bot_command is False:
                    time_code = ""
                    print(message.author)
                    if str(message.author) in pdt_residents:
                        time_code = "America/Los_Angeles"
                    elif str(message.author) in cdt_residents:
                        time_code = "America/Chicago"
                    elif str(message.author) in edt_residents:
                        time_code = "America/New_York"
                    elif str(message.author) in pht_residents:
                        time_code = "PST"
                else:
                    time_code = "America/Los_angeles"
                print(f'time_code: {time_code}')

                if len(time_parse) > 0:
                    print(f'{time_parse[0]} {time_code}')
                    time_pdt = SimpleDate(
                        f'{time_parse[0]} {time_code}').convert(
                            format='I:Mp     m/d',
                            tz='America/Los_Angeles',
                            is_dst=True)
                    time_cdt = SimpleDate(
                        f'{time_parse[0]} {time_code}').convert(
                            format='I:Mp     m/d',
                            tz='America/Chicago',
                            is_dst=True)
                    time_edt = SimpleDate(
                        f'{time_parse[0]} {time_code}').convert(
                            format='I:Mp     m/d',
                            tz='America/New_York',
                            is_dst=True)
                    time_pht = SimpleDate(
                        f'{time_parse[0]} {time_code}').convert(
                            format='I:Mp     m/d', country='PH')
                else:
                    print("inside else. len <= 0")
                    time_pdt = SimpleDate(f'{time_parse} {time_code}').convert(
                        format='I:Mp     m/d',
                        tz='America/Los_Angeles',
                        is_dst=True)
                    time_cdt = SimpleDate(f'{time_parse} {time_code}').convert(
                        format='I:Mp     m/d',
                        tz='America/Chicago',
                        is_dst=True)
                    time_edt = SimpleDate(f'{time_parse} {time_code}').convert(
                        format='I:Mp     m/d',
                        tz='America/New_York',
                        is_dst=True)
                    time_pht = SimpleDate(f'{time_parse} {time_code}').convert(
                        format='I:Mp     m/d', country='PH')

                embedVar = discord.Embed(color=0x5a9ef4)
                embedVar.add_field(name="PDT",
                                   value=str(time_pdt).lower(),
                                   inline=False)
                embedVar.add_field(name="CDT",
                                   value=str(time_cdt).lower(),
                                   inline=False)
                embedVar.add_field(name="EDT",
                                   value=str(time_edt).lower(),
                                   inline=False)
                embedVar.add_field(name="PHT",
                                   value=str(time_pht).lower(),
                                   inline=False)
                await message.reply(embed=embedVar, mention_author=False)

                # Randomly send message
                if random.randrange(0, 101) == 69:
                    await message.channel.send(
                        "NICO NICO NIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")

            except Exception as e:
                print(f'Exception: {e}')

        if config["keyword1"] in message.content.lower():
            await message.channel.send(random.choice(keyword1_message))
        elif config["keyword2"] in message.content.lower():
            await message.channel.send(keyword2_message)
        elif config["keyword3"] in message.content.lower():
            await message.channel.send(keyword2_message)
        elif config["keyword4"] in message.content.lower():
            await message.channel.send(keyword4_message)
        elif config["keyword5"] in message.content.lower():
            await message.channel.send(keyword5_message)
        elif config["keyword6"] in message.content.lower():
            await message.channel.send(keyword6_message)
        elif config["keyword7"] in message.content.lower():
            await message.channel.send(keyword7_message)
        elif config["keyword8"] in message.content.lower():
            await message.channel.send(keyword8_message)
        elif config["keyword9"] in message.content.lower():
            await message.channel.send(keyword9_message)
        elif config["keyword10"] in message.content.lower():
            await message.channel.send(keyword10_message)
        elif config["keyword11"] in message.content.lower():
            await message.channel.send(keyword11_message)


client = MyClient()
client.run(config["token"])
